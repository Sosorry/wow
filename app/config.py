import os

from starlette.config import Config
from starlette.datastructures import CommaSeparatedStrings, Secret

#dir_path = os.path.dirname(os.path.realpath(__file__))
#root_dir = dir_path
#config = Config(f'{root_dir}.env')

#DATABASE_URL = f'sqlite:///{root_dir}' + config('DB_NAME', cast=str)
dir_path = os.path.dirname(os.path.realpath(__file__))
root_dir = os.path.abspath(os.path.join(dir_path, '..'))  #  os.path.join для безопасного объединения путей
config = Config(os.path.join(root_dir, '.env'))

DATABASE_URL = f'sqlite:///{root_dir}/' + config('DB_NAME', cast=str) 
SECRET_KEY = config('SECRET_KEY', cast=Secret)
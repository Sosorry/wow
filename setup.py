from setuptools import setup, find_packages

setup(
    name='app-example',
    version='2.2.8',
    author='chepuha',
    description='FastApi app',
    install_requires=[
        'fastapi==0.105.0',
        'uvicorn==0.24.0.post1',
        'SQLAlchemy==2.0.23',
        'pytest==7.4.3',
        'requests==2.31.0',
    ],
    scripts=['app/main.py', 'scripts/create_db.py']
)